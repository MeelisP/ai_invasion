/**
 * Created by Meelis Perli on 6/24/2016.
 */

function enemy(x,y) {
    //basics
    this.x = x;
    this.y = y;
    this.side = 25;
    this.speed = 1000;
    this.color = "#ff0000";
    this.hp = 1;
    this.dead = false;

    //attack stuff
    this.damage = 25;
    this.hitted = false;
    this.hitCooldownTimer = getTime2();
    this.hitCooldown = 1000;

    //neurons
    this.score = 0;
    this.outputNeurons = [0, 0];
    this.eyes = [0, 0, 0, 0, 0, 0, 0, 0]; //8 eyes atm. eye can have value -1, 0 or 1. -1 means it sees bullet. 1 means it sees player and 0 if it doesn't see anything
    this.brain = new brain(this.eyes.length,[5,3],this.outputNeurons.length);
    this.middleLayerNeurons = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    this.draw = function () {
        ctx.fillStyle = this.color;
        ctx.fillRect(this.x - this.side / 2, this.y - this.side / 2, this.side, this.side);
    };

    this.update = function () {
        //get data using it's 8 eyes
        this.eyes = [0, 0, 0, 0, 0, 0, 0, 0];
        for (var i = 0; i < this.eyes.length; i++) {
            var a = Math.atan2(this.y - player.y, this.x - player.x) + Math.PI;
            if (a > Math.PI * (i) / 4 && a < Math.PI * (i + 1) / 4) {
                this.eyes[i] = 1;
            }
            for (var j = 0; j < bullets.length; j++) {
                var bullet = bullets[j];
                var bulletA = Math.atan2(this.y - bullet.y, this.x - bullet.x) + Math.PI;
                if (bulletA > Math.PI * (i) / 4 && bulletA < Math.PI * (i + 1) / 4) {
                    var dis = Math.sqrt(Math.pow(this.x - bullet.x, 2) + Math.pow(this.y - bullet.y, 2));
                    if (dis < 200) {
                        this.eyes[i] = -(1 - dis / 200);
                    } else {
                        this.eyes[i] = -1;
                    }
                }
            }
        }
        this.outputNeurons = this.brain.calculateOutputs(this.eyes);

        //movement
        if (this.x + this.side / 2 + this.outputNeurons[0] * this.speed < WIDTH && this.x - this.side / 2 + this.outputNeurons[0] * this.speed > 0) {
            this.x += this.outputNeurons[0] * this.speed;
        } else if (this.x + this.outputNeurons[0] * this.speed > WIDTH) {
            this.x = WIDTH;
        } else if (this.x + this.outputNeurons[0] * this.speed < 0)
            this.x = 0;

        if (this.y + this.side / 2 + this.outputNeurons[1] * this.speed < HEIGHT && this.y - this.side / 2 + this.outputNeurons[1] * this.speed > 0) {
            this.y += this.outputNeurons[1] * this.speed;
        } else if (this.y + this.outputNeurons[1] * this.speed > HEIGHT) {
            this.y = HEIGHT;
        } else if (this.y + this.outputNeurons[1] * this.speed < 0)
            this.y = 0;

        //collisions with bullets

        for (i = 0; i < bullets.length; i++) {
            var bullet = bullets[i];

            if (this.x - this.side / 2 < bullet.x && this.x + this.side / 2 > bullet.x &&
                this.y - this.side / 2 < bullet.y && this.y + this.side / 2 > bullet.y) {
                bullets.splice(i, 1);
                this.hp -= bullet.damage;
                this.score -= 1;

            }
        }

        if (this.hitCooldownTimer < getTime2()) {
            this.hitted = false;
        }
        //collisions with player
        if (this.x - this.side / 2 < player.x + player.side / 2 && this.x + this.side / 2 > player.x - player.side / 2 &&
            this.y - this.side / 2 < player.y + player.side / 2 && this.y + this.side / 2 > player.y - player.side / 2 && this.hitted == false) {
            player.hp -= this.damage + getRandomInt(-5, 5);
            this.hitted = true;
            this.hitCooldownTimer = this.hitCooldown + getTime2();
            this.score += 5;
        }


        if (this.hp <= 0) {
            this.dead = true;
            this.score += 3 - Math.sqrt(Math.pow(this.x - player.x, 2) + Math.pow(this.y - player.y, 2)) / 100;
            deadCount += 1;

        }
    }
}

function brain(inputsN, hiddenLayersN, outputsN){
    //inputsN: number of input neurons
    //hiddenLayers: array that has at least 1 element (number of neurons on that layer)
    //outputsN: number of outputs
    this.inputsN = inputsN;
    this.hiddenLayersN = hiddenLayersN;
    this.outputsN = outputsN;
    this.weights = [];
    this.hiddenLayers = [];

    //==================================================================================================================
    //build brain (generates weights)
    //==================================================================================================================

    //generating weights, which are connected to inputs
    var w1 = [];
    for (var i = 0; i < this.hiddenLayersN[0]; i++){
        var w2 = [];
        for (var j = 0; j < this.inputsN; j++){
            w2.push(getRandomInt(-120, 120) / 1000)
        }
        w1.push(w2);
    }
    this.weights.push(w1);
    //connecting hidden layer neurons
    for (var i = 0; i < hiddenLayersN.length-1; i++){
        var w1 = [];
        for (var j = 0; j < this.hiddenLayersN[i]; j++){
            var w2 = [];
            for (var k = 0; k < this.hiddenLayersN[i+1]; k++){
                w2.push(getRandomInt(-120, 120) / 1000)
            }
            w1.push(w2);
        }
        this.weights.push(w1);
    }
    //connecting outputs
    var w1 = [];
    for (var i = 0; i < this.outputsN; i++) {
        var w2 = [];
        for (var j = 0; j < this.hiddenLayersN[this.hiddenLayersN.length-1]; j++) {
            w2.push(getRandomInt(-120, 120) / 1000)
        }
        w1.push(w2);
    }
    this.weights.push(w1);

    //constructing hidden layer neurons
    for (var i = 0; i < hiddenLayersN.length; i++){
        var n = [];
        for (var j = 0; j < hiddenLayersN[i]; j++){
            n.push(0);
        }
        this.hiddenLayers.push(n);
    }
    //console.log(this.weights);
    //console.log(this.weights[2]);
    //console.log(this.weights[2][1]);

    //==================================================================================================================
    //calculations
    //==================================================================================================================

    this.calculateOutputs = function(inputs) {
        //calculating first hidden layer neuron values
        for (var i = 0; i < this.hiddenLayersN[0]; i++) {
            var sum = 0;
            for (var j = 0; j < this.inputsN; j++) {
                sum += this.weights[0][i][j] * inputs[j];
            }
            this.hiddenLayers[0][i] = sum;
        }

        //calculating rest of the hidden layer neurons;
        for (var i = 0; i < hiddenLayersN.length - 1; i++) {
            for (var j = 0; j < this.hiddenLayersN[i+1]; j++) {
                var sum = 0;
                for (var k = 0; k < this.hiddenLayersN[i]; k++) {
                    sum += this.weights[i+1][k][j] * this.hiddenLayers[i][k];

                }
                this.hiddenLayers[i+1][j] = sum;
            }
        }

        //calculation outputs
        var outputs = [];
        for (var i = 0; i < this.outputsN; i++) {
            var sum = 0;
            for (var j = 0; j < this.hiddenLayersN[this.hiddenLayersN.length-1]; j++) {
                sum += this.weights[this.weights.length-1][i][j] * this.hiddenLayers[this.hiddenLayers.length-1][j];
            }
            outputs.push(sum);
        }
        return outputs;
    }

}


function evolve(){
    //sorting enemies by their score
    enemies.sort(function(a, b) {
        return parseFloat(b.score) - parseFloat(a.score);
    });

    var n;
    //selecting number of enemies, who will be used to create new ones.
    if (enemies.length % 2 == 0){
        if (enemies.length <= evolveN){
            n = enemies.length;

        }else{
            n = evolveN;
        }
    }else{
        if (enemies.length <= evolveN){
            n = enemies.length-1;

        }else{
            n = evolveN;
        }
    }

    //evolution
    //for (var i = 0; n; i++){
    //  console.log(enemies[i].score);
    //}
}



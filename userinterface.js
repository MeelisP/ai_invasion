/**
 * Created by Meelis Perli on 6/29/2016.
 */

function drawUserInterface(){
    drawHPBar(25,40);
    drawGeneration(25,30)
    
}

function drawHPBar(x,y){
    ctx.fillStyle = "#000000";
    ctx.fillRect(x,y,100,15);
    ctx.fillStyle = "#555555";
    ctx.fillRect(x+2,y+2,96,11);
    if (player.hp/player.maxhp > 0.66){
        ctx.fillStyle = "#00ff00";
    }else if (player.hp/player.maxhp <= 0.66 && player.hp/player.maxhp >= 0.33){
        ctx.fillStyle = "#ffff00";
    }else{
        ctx.fillStyle = "#ff0000";
    }

    ctx.fillRect(x+2,y+2,96*player.hp/player.maxhp,11);
}
function drawGeneration(x,y){
    ctx.font = "25px Arial";
    ctx.fillStyle = "#000000";
    ctx.fillText("Generation: "+String(gen),x,y);
}
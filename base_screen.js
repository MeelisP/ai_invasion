/**
 * Created by Meelis Perli on 7/19/2016.
 */

startBtnCommand = function(){
    evolve();
    gameScreen = "level";
    clickLocation = [-1,-1];
};


function button(text, x, y, width, height,funct){
    this.x = x;
    this.y = y;
    this.text = text;
    this.width = width;
    this.height = height;
    this.fontSize = 25;
    this.hover = false;


    this.draw = function(){
        ctx.fillStyle = "#FFA003";
        ctx.fillRect(this.x,this.y,this.width,this.height);
        ctx.font = String(this.fontSize)+"px Arial";
        ctx.fillStyle = "#000000";
        ctx.fillText(this.text,this.x + this.width/2 - ctx.measureText(this.text).width/2,this.y + this.height - this.fontSize);
        if (this.hover && !this.pressed){
            ctx.globalAlpha = 0.2;
            ctx.fillStyle = "#ffffff";
            ctx.fillRect(x,y,this.width,this.height);
        }
        else if(this.pressed){
            ctx.globalAlpha = 0.2;
            ctx.fillStyle = "#000000";
            ctx.fillRect(x,y,this.width,this.height);
        }
        ctx.globalAlpha = 1;
    };
    this.update = function(){
        //hovering
        if (this.x < mousePos[0] && this.x + this.width > mousePos[0] && this.y < mousePos[1] && this.y + this.height > mousePos[1] ){
            this.hover = true;
        }
        else{
            this.hover = false;
        }
        if (this.x < clickLocation[0] && this.x + this.width > clickLocation[0] && this.y < clickLocation[1] && this.y + this.height > clickLocation[1] && pressed){
            this.pressed = true;
        }
        else{
            this.pressed = false;
        }
        if (this.x < clickLocation[0] && this.x + this.width > clickLocation[0] && this.y < clickLocation[1] && this.y + this.height > clickLocation[1] && !pressed && this.hover){
            funct();
        }
    }
}

function BaseScreen(){
    
    baseBtns.push(new button("begin wave", WIDTH/2 - 100,100,200,64,startBtnCommand));//start button

    this.update = function() {
        for (var i = 0; i < baseBtns.length; i++){
            baseBtns[i].update();
        }

    };

    this.draw = function() {
        ctx.globalAlpha = 1;
        ctx.fillStyle = "#555555";
        ctx.fillRect(0,0,WIDTH,HEIGHT);
        drawGeneration(25,30);
        for (var i = 0; i < baseBtns.length; i++){
            baseBtns[i].draw();
        }
    };
}

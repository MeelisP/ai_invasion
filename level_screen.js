/**
 * Created by Meelis Perli on 7/19/2016.
 */

function LevelScreen(){
    
    this.update = function() {
        player.update();
        if (bullets.length > 0) {
            for (var i = 0; i < bullets.length; i++) {
                bullets[i].update();
            }
        }
        if (enemies.length > 0) {
            for (var i = 0; i < enemies.length; i++) {
                enemy = enemies[i];
                if (!enemy.dead) {
                    enemy.update();
                }
            }
        }
        if (enemies.length == deadCount){
            gen++;
            gameScreen = "basef";
            
        }
    };
    
    this.draw = function(){
        ctx.fillStyle = "#00a500";
        ctx.fillRect(0,0,WIDTH,HEIGHT);
        player.draw();
        if (bullets.length > 0) {
            for (var i = 0; i < bullets.length; i++){
                bullets[i].draw();
            }
        }
        if (enemies.length > 0) {
            for (var i = 0; i < enemies.length; i++){
                enemy = enemies[i];
                if (!enemy.dead){
                    enemy.draw();
                }
            }
        }

        drawUserInterface();
    }
}
   
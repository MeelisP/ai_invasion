/**
 * Created by Meelis Perli on 6/24/2016.
 */
var pressed = false;

player = {
    x: null,
    y: null,
    side: 25,
    color: "#000",
    speed: 10,
    maxhp: 100,
    hp: 100,
    weapon: "pistol",

    update: function() {
        if (keyState[keyW] && this.y > 0+this.side/2) this.y -= this.speed;
        if (keyState[keyS] && this.y < HEIGHT-this.side/2) this.y += this.speed;
        if (keyState[keyD] && this.x < WIDTH-this.side/2) this.x += this.speed;
        if (keyState[keyA] && this.x > 0+this.side/2) this.x -= this.speed;
        if (pressed && shoot) {
                shoot = false;
                bullets.push(new bullet(this.x,this.y,5,10,clickLocation));
        }
        
    },
    draw: function(){
        ctx.fillStyle = this.color;
        ctx.fillRect(this.x-this.side/2,this.y-this.side/2,this.side,this.side);
    }
};
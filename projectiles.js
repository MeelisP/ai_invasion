/**
 * Created by Meelis Perli on 6/24/2016.
 */

function bullet(x,y,side,speed,targetLoc){
    this.x = x;
    this.y = y;
    this.side = side;
    this.color = "#000000";
    this.damage = 50;
    this.acc = 10;
    var randomAngle = getRandomInt(-this.acc,this.acc)/57.3;
    console.log(Math.atan2(y-targetLoc[1],x-targetLoc[0]));
    this.speedX = speed*Math.cos(Math.atan2(y-targetLoc[1],x-targetLoc[0])+randomAngle);
    this.speedY = speed*Math.sin(Math.atan2(y-targetLoc[1],x-targetLoc[0])+randomAngle);
    this.draw = function(){
        ctx.fillStyle = this.color;
        ctx.fillRect(this.x-this.side/2,this.y-this.side/2,this.side,this.side);
    };
    this.update = function(){
        this.x -= this.speedX;
        this.y -= this.speedY;
        if (this.x < 0 || this.x > WIDTH || this.y < 0 || this.y > HEIGHT){
            bullets.splice(bullets.indexOf(this),1);
        }
    }
}

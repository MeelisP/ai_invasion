/**
 * Created by Meelis Perli on 6/24/2016.
 */

var WIDTH = 1200, HEIGHT = 800;
var keyW = 87, keyS = 83, keyD = 68, keyA = 65;
var gAlpha = 0;
var canvas, ctx, keyState, clickLocation = [0,0];
var bullets = [], enemies = [], baseBtns = [], mousePos = [0,0];
var pressed, shoot = true;
var numberOfAI = 1, deadCount = 0, evolveN = 10; //evolveN: max number of enemies, that will be used to create new enemies
var level = new LevelScreen();
var base = new BaseScreen();
player;
//game data
var gameScreen = "base";
var gen = 0;

function main(){
    canvas = document.createElement("canvas");
    canvas.width = WIDTH;
    canvas.height = HEIGHT;
    ctx = canvas.getContext("2d");
    document.body.appendChild(canvas);

    keyState = {};
    document.addEventListener("keydown", function(evt){
        keyState[evt.keyCode] = true;
    });
    document.addEventListener("keyup", function(evt){
        delete keyState[evt.keyCode];
    });
    document.addEventListener("mousedown", function(evt){
        var rect = canvas.getBoundingClientRect();
        pressed = true;
        clickLocation = [Math.round((evt.clientX-rect.left)/(rect.right-rect.left)*canvas.width),
                         Math.round((evt.clientY-rect.top)/(rect.bottom-rect.top)*canvas.height)];
        });
    document.addEventListener("mouseup", function(evt){
        shoot = true;
        pressed = false;
    });
    document.addEventListener("mousemove", function(evt){
        var rect = canvas.getBoundingClientRect();
        mousePos = [Math.round((evt.clientX-rect.left)/(rect.right-rect.left)*canvas.width),
                    Math.round((evt.clientY-rect.top)/(rect.bottom-rect.top)*canvas.height)];
    });


    init();
    var loop = function(){
        update();
        draw();
        window.requestAnimationFrame(loop, canvas);
    };
    window.requestAnimationFrame(loop, canvas);
}

function init(){
    
    player.x = WIDTH/2;
    player.y = HEIGHT/2;
    for (var i = 0; i < numberOfAI; i++){
        enemies.push(new enemy(getRandomInt(100,WIDTH-100),getRandomInt(100,HEIGHT-100)));
    }
    

}
function fade(){
    ctx.globalAlpha = 1;
    ctx.fillStyle = "#00a500";
    ctx.fillRect(0,0,WIDTH,HEIGHT);
    player.draw();
    gAlpha+= 0.03;
    ctx.globalAlpha = gAlpha;
    ctx.fillStyle = "#000000";
    ctx.fillRect(0,0,WIDTH,HEIGHT);
    if (gAlpha >= 1){
        gameScreen = gameScreen.substring(0, gameScreen.length - 1);
    }
}

function update(){
    if (gameScreen == "level"){
        level.update();
    }
    else if (gameScreen == "base"){

        base.update();
    }
    
}
function draw(){
    if (gameScreen == "basef"){
        fade();

    }
    else if (gameScreen == "level"){
        level.draw();
    }
    else if (gameScreen == "base"){

        base.draw();
    }

    ctx.restore();

}

main();